package com.example.application_seven

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),OnSeekBarChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        seek_bar.setOnSeekBarChangeListener(this)
        exit_buuton.setOnClickListener {
            finish()
        }
        radio_group.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val radio = findViewById<RadioButton>(checkedId)
            when(radio){
                radio_1 -> tv_show.typeface = Typeface.SERIF
                radio_2 -> tv_show.typeface =  Typeface.SANS_SERIF
            }
        })
        checkbox_bold.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                tv_show.typeface = Typeface.DEFAULT_BOLD
            }
        }
        checkbox_italic.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                tv_show.typeface = Typeface.defaultFromStyle(Typeface.ITALIC)
            }
        }

        editText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                tv_show.text = editText.text
            }
        })

    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        //Toast.makeText(this,progress.toString(),Toast.LENGTH_LONG).show()
        tv_show.textSize = progress.toFloat()
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {

    }


}


